import sqlite3
conn = sqlite3.connect("./banco.db")


def connect():
    try:
        cursor = conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS Aluno(
                        matricula TEXT PRIMARY KEY NOT NULL,
                        nome TEXT NOT NULL,
                        av1 TEXT NOT NULL,
                        av2 TEXT NOT NULL,
                        av3 TEXT NOT NULL,
                        avd TEXT NOT NULL,
                        avds TEXT NOT NULL,
                        email TEXT NOT NULL,
                        endereco TEXT NOT NULL,
                        campus TEXT NOT NULL,
                        periodo TEXT NOT NULL)''')
        conn.commit()
    except Exception as e:
        print("Erro na criação da tabela: ", e)


def insert(matricula, nome, av1, av2, av3, avd, avds, email, endereco, campus, periodo):
    try:
        cursor = conn.cursor()
        cursor.execute('INSERT INTO Aluno (matricula, nome, av1, av2, av3, avd, avds, '
                       'email, endereco, campus, periodo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                       (matricula, nome, av1, av2, av3, avd, avds, email, endereco, campus, periodo))
        conn.commit()
    except sqlite3.Error as e:
        print("Erro ao cadastrar o aluno: ", e)
    finally:
        conn.close()


def view():
    try:
        cursor = conn.cursor()
        cursor.execute('SELECT matricula, nome, av1, av2, av3, avd, avds, email, endereco, campus, periodo '
                            'FROM Aluno')
    except sqlite3.Error as e:
        print("Erro ao buscar o aluno: ", e)
    finally:
        linha = cursor.fetchall()
        conn.close()
        return linha


def delete(matricula):
    try:
        cursor = conn.cursor()
        cursor.execute('DELETE FROM Aluno WHERE matricula = ?',(matricula, ))
        conn.commit()
        conn.execute('VACUUM')
    except sqlite3.Error as e:
        print('Erro ao excluir aluno: ', e)
    finally:
        conn.close()


def update(matricula, nome, av1, av2, av3, avd, avds, email, endereco, campus, periodo):
    try:
        cursor = conn.cursor()
        cursor.execute('UPDATE Aluno SET nome=?, av1=?, av2=?, av3=?, avd=?, avds=?, email=?, endereco=?, '
                          'campus=?, periodo = ? WHERE matricula = ?',
                          (matricula, nome, av1, av2, av3, avd, avds, email, endereco, campus, periodo))
        conn.commit()
        print('Cadastro atualizado com sucesso.')
    except sqlite3.Error as e:
        print('Erro ao atualizar o cadastro do aluno: ', e)
    finally:
        conn.close()


connect()

